#include "monitor.h"

monitor::monitor(int nc, int sc, int tot) {
  pthread_mutex_init(&lock, NULL);
  pthread_mutex_init(&check, NULL);

  northCars = nc;
  southCars = sc;
  total = tot;
  southCarsOnBridge = 0;
  northCarsOnBridge = 0;
  carsCrossed = CARS_BEFORE_SWAP;
  currentDirection = 'N';
}

monitor::~monitor() {
  // Nothing because nothing to save.
}

bool monitor::clear(char direction) {
  pthread_mutex_lock(&lock);
  if (direction == 'N') {
    if (currentDirection == 'N' && northCarsOnBridge < 5) {
      northCarsOnBridge++;
      pthread_mutex_unlock(&lock);
      return true;
    }
    else {
      pthread_mutex_unlock(&lock);
      return false;
    }
  } else {
    if (currentDirection == 'S' && southCarsOnBridge < 5) {
      southCarsOnBridge++;
      pthread_mutex_unlock(&lock);
      return true;
    }
    else {
      pthread_mutex_unlock(&lock);
      return false;
    }
  }
}

void monitor::swap(char direction) {
  pthread_mutex_lock(&check);
  if (direction == 'N' ) {
    currentDirection = 'S';
    carsCrossed = CARS_BEFORE_SWAP;
  }
  else if (direction == 'S') {
    currentDirection = 'N';
    carsCrossed = CARS_BEFORE_SWAP;
  }
  printf("DIRECTION IS %c\n", currentDirection);
  pthread_mutex_unlock(&check);
}

void monitor::done(char direction) {
  pthread_mutex_lock(&check);
  if (direction == 'N') {
    northCarsOnBridge--;
    carsCrossed--;
    northCars--;
    southCars++;
  }
  else{
    southCarsOnBridge--;
    carsCrossed--;
    southCars--;
    northCars++;
  }

  if (carsCrossed <= 0 || (!northCars || !southCars)) {
    pthread_mutex_unlock(&check);
    swap(direction);
  }
  else
    pthread_mutex_unlock(&check);
}

void monitor::end(char direction) {
  pthread_mutex_lock(&lock);
  if (direction == 'N') {
    northCars--;
  } else if (direction == 'S'){
    southCars--;
  }
  total--;

  if (!northCars)
    swap('N');
  else if(!southCars)
    swap('S');
  pthread_mutex_unlock(&lock);
}
