#include <iostream>
#include <pthread.h>
#include <thread>
#include <unistd.h>
#include <time.h>

#define MAX_WORKERS 4
#define MAX_TRIPS 10;

int trips;

void * northCar(void *);
void * southCar(void *);

void createCars(long nc, long sc, int trips) {
  long i;
  pthread_t northCarsId[nc];
  pthread_t southCarsId[sc];

  for (i = 0; i < nc; i++) {
    // pthread_create(&northCarsId[i], NULL, northCar, (void *) i);
    std::thread nc(&northCar);
  }
  for (i = 0; i < sc; i++) {
    pthread_create(&southCarsId[i], NULL, southCar, (void *) i);
  }
}

// void north(int id, int trips) {
//   int value, sleep = ((rand()%9) + 1) * 1000;
//   // sem_wait(&check);
//   // sem_getvalue(&s, &value);
//   // Check if bridge is empty
//   if (value == MAX_CARS_ON_BRIDGE) {
//     // sem_wait(&n);
//
//     // Add car to queue on bridge
//     carsOnBridge[lastCar] = id;
//     queueAdd();
//
//     // sem_post(&check);
//     printf("CAR %d, CROSSING BRIDGE NORTHBOUND FOR %dms\n", id, sleep/1000);
//     usleep(sleep); // Travel
//
//     // Wait to exit
//     while (1) {
//       // sem_wait(&queue);
//       // Check if it's your turn to exit
//       if (id == carsOnBridge[nextCar]) {
//         queueRemove();
//         // sem_post(&queue);
//         break; // Exit
//       }
//       // sem_post(&queue);
//       usleep(1000);
//     }
//
//     trips--;
//     // sem_post(&n);
//     wait('S', id, trips);
//   }
//   else {
//     // Bridge not empty, go back to waiting
//     // sem_post(&check);
//     wait('N', id, trips);
//   }
// }

// void end(int id) {
//   printf("CAR %d TRIP IS DONE!\n", id);
//   // sem_wait(&lock);
//   // cars--;
//   // if (cars == 0)
//     // sem_post(&done);
//   // sem_post(&lock);
//   pthread_exit(NULL);
// }

// void wait(char direction, int id, int trips) {
//   int s = ((rand()%45) + 1) * 1000;
//   if (trips == 0)
//     end(id);
//
//   if (direction == 'N') {
//     usleep(s);
//     north(id, trips); // 1
//   }
//   else if(direction == 'S'){
//     usleep(s);
//     south(id, trips); // 0
//   }
// }

int main(int argc, char const *argv[]) {
  srand (time(NULL));
  trips = MAX_TRIPS;

  createCars(MAX_WORKERS, MAX_WORKERS, MAX_WORKERS);
  sleep(20);
  return 0;
}

void * northCar() {
  // uintptr_t threadId = (uintptr_t)arg;
  // int id = (int) threadId;
  int tripsLeft = trips;
  char direction = 'N';

  // wait(id, direction, tripsLeft);
  printf("%s\n", direction);
}

void * southCar(void * arg) {
  uintptr_t threadId = (uintptr_t)arg;
  int id = (int) threadId;
  int tripsLeft = trips;
  char direction = 'S';

  // wait(id, direction, tripsLeft);
}
