#include <pthread.h>
#include <iostream>
#include <string>
#include <unistd.h>

class monitor {
  public:
    monitor(int nc, int sc, int tot); // Constructor
    ~monitor(); // Destructor

    bool clear(char direction);
    void swap(char direction);
    void done(char direction);
    void end(char direction);

    const static int MAX_CARS_ON_BRIDGE = 5;
    const static int CARS_BEFORE_SWAP = 3;

  private:
    pthread_mutex_t lock;
    pthread_mutex_t check;
    pthread_cond_t bridge_empty;

    int northCars;
    int southCars;
    int total;
    int northCarsOnBridge;
    int southCarsOnBridge;
    int carsCrossed;
    char currentDirection;
};
