#include <iostream>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include "monitor.h"

#define MAX_CARS 20
#define MAX_TRIPS 40
#define MAX_CARS_ON_BRIDGE 5

void * northCar(void * arg);
void * southCar(void * arg);
void wait(std::string id, char direction, int tripsLeft);


int trips;
// int northCars, southCars;

pthread_t cars[MAX_CARS*2];

monitor * mon;

/* CREATE AND JOIN */
void createCars(int nc, int sc) {
  long i;
  for (i = 0; i < nc; i++)
    pthread_create(&cars[i], NULL, northCar, (void *) i);
  for (i = 0; i < sc; i++)
    pthread_create(&cars[i+sc], NULL, southCar, (void *) i);
}
void joinCars(int totaltCars) {
  int i;
  for (i = 0; i < totaltCars * 2; i++)
    pthread_join(cars[i], NULL);
}
/* END CREATE AND JOIN */

/* TRAVEL */
void end(std::string id, char direction) {
  printf("CAR %s TRIPS DONE\n", id.c_str());
  mon->end(direction);
}

void north(std::string id, char direction, int tripsLeft) {
  int s = ((rand() % 9) + 1) * 1000;
  if (mon->clear(direction)) {
    printf("CAR %s CROSSING BRIDGE FOR %dms NORTH\n", id.c_str(), s/1000);
    usleep(s);

    printf("CAR %s EXITING BRIDGE\n", id.c_str());
    mon->done(direction);
    tripsLeft--;
    wait(id, 'S', tripsLeft);
  }
  else {
    wait(id, direction, tripsLeft);
  }
}

void south(std::string id, char direction, int tripsLeft) {
  int s = ((rand() % 9) + 1) * 1000;
  if (mon->clear(direction)) {
    printf("CAR %s CROSSING BRIDGE FOR %dms SOUTH\n", id.c_str(), s/1000);
    usleep(s);

    printf("CAR %s EXITING BRIDGE\n", id.c_str());
    mon->done(direction);
    tripsLeft--;
    wait(id, 'N', tripsLeft);
  }
  else {
    wait(id, direction, tripsLeft);
  }
}

void wait(std::string id, char direction, int tripsLeft) {
  int s = ((rand() % 45) + 1) * 1000;
  if (!tripsLeft)
    end(id, direction);

  else if (direction == 'N') {
    printf("CAR %s, WAITING %dms TO CROSS NORTHBOUND\n", id.c_str(), s/1000);
    usleep(s);
    north(id, direction, tripsLeft);
  }
  else {
    printf("CAR %s, WAITING %dms TO CROSS SOUTHBOUND\n", id.c_str(), s/1000);
    usleep(s);
    south(id, direction, tripsLeft);
  }
}
/* END TRAVEL */
/* INIT */
int main(int argc, char const *argv[]) {
  int nc, sc;


  nc = (argc > 1)? atoi(argv[1]) : MAX_CARS;
  sc = (argc > 2)? atoi(argv[2]) : MAX_CARS;
  trips = (argc > 3)? atoi(argv[3]) : MAX_TRIPS;
  if (nc > MAX_CARS) nc = MAX_CARS;
  if (sc > MAX_CARS) sc = MAX_CARS;
  if (trips > MAX_TRIPS) trips = MAX_TRIPS;

  mon = new monitor(nc, sc, nc+sc);
  createCars(nc, sc);
  joinCars((nc+sc));
  delete mon;
  return 0;
}
/* INIT CARS */
void * northCar(void * arg) {
  uintptr_t temp = (uintptr_t) arg;
  char direction = 'N';
  int tripsLeft = trips;
  int id = (int) temp;
  std::string name = "N";
  name += std::to_string(id);
  // printf("%s\n", name.c_str());
  printf("CAR %s\n", name.c_str());
  wait(name, direction, trips);
}
void * southCar(void * arg) {
  uintptr_t temp = (uintptr_t) arg;
  char direction = 'S';
  int tripsLeft = trips;
  int id = (int) temp;
  std::string name = "S";
  name += std::to_string(id);
  // printf("%s\n", name.c_str());
  printf("CAR %s\n", name.c_str());
  wait(name, direction, trips);
}
/* END INIT CARS */
/* END INIT */
